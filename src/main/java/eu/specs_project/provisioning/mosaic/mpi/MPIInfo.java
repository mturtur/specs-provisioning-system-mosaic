package eu.specs_project.provisioning.mosaic.mpi;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MPIInfo {

	private final UUID id;
	private Set<UUID> jobs;
	private UUID mpiEnvId;
	private String MPIUrl; //TODO eliminare URL, settare mpiEnvId
	private String privateKeyMPIUser, publicKeyMPIUser, mpiUserUsername;
	
	public MPIInfo(UUID id, String mpiUser){
		this.id=id;
		this.mpiUserUsername=mpiUser;
		jobs = new HashSet<UUID>();
	}

	public String getPrivateKeyMPIUser() {
		return privateKeyMPIUser;
	}

	public void setPrivateKeyMPIUser(String privateKeyMPIUser) {
		this.privateKeyMPIUser = privateKeyMPIUser;
	}
	
	public String getPublicKeyMPIUser() {
		return publicKeyMPIUser;
	}

	public void setPublicKeyMPIUser(String publicKeyMPIUser) {
		this.publicKeyMPIUser = publicKeyMPIUser;
	}

	public UUID getId() {
		return id;
	}

	public UUID getMpiEnvId() {
		return mpiEnvId;
	}

	public String getMpiUserUsername() {
		return mpiUserUsername;
	}

	public String getMPIUrl() {
		return MPIUrl;
	}

	public void setMPIUrl(String mPIUrl) {
		MPIUrl = mPIUrl;
	}
	
	public void addJob(UUID command){
		jobs.add(command);
	}
	
}
