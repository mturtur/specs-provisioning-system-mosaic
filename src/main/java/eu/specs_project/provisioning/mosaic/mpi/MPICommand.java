package eu.specs_project.provisioning.mosaic.mpi;

import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import eu.specs_project.provisioning.mosaic.temp.ClusterNode;

public class MPICommand {

	public MPICommand(String jobName, String userName, List<ClusterNode> workerHosts, int numberOfProcesses, String executable, String parameters){
		id = UUID.randomUUID();
		this.mpiUser=userName;
		this.jobName=jobName;
		this.launched = (GregorianCalendar) GregorianCalendar.getInstance();
		this.procNumber=numberOfProcesses;
		this.executable=executable;
		this.parameters = parameters;
		this.outputFile = "/home/"+userName+"/"+id+".txt";
		this.output="";
		StringBuilder mpirunCommand = new StringBuilder();
		
		mpirunCommand.append(commandInit.replace("£1", String.valueOf(numberOfProcesses)));
		this.workerHosts=workerHosts;
		for(ClusterNode n : workerHosts){
			mpirunCommand.append(" -host "+n.getPrivateIP());
		}
		mpirunCommand.append(" /mpi/"+executable+" "+parameters+" > "+outputFile);
		this.commandFinal = mpirunCommand.toString();
	}
	
	public UUID getId(){
		return id;
	}
	
	public String getCommandFinal(){
		return this.commandFinal;
	}
	
	public void setOutput(String output){
		this.output=output;
	}
	
	public String getOutput(){
		return output;
	}
	
	public List<ClusterNode> getHosts() {
		return workerHosts;
	}

	public GregorianCalendar getLaunched() {
		return launched;
	}

	public GregorianCalendar getEnded() {
		return ended;
	}

	public void setEnded(GregorianCalendar ended) {
		this.ended = ended;
	}
	
	public void setJobState(JobState state){
		this.jobState=state;
	}
	
	public JobState getJobState(){
		return jobState;
	}

	public int getProcNumber() {
		return procNumber;
	}

	public String getJobName() {
		return jobName;
	}
	
	public String getOutputFileName(){
		return outputFile;
	}
	
	public String getExecutable() {
		return executable;
	}

	public String getParameters() {
		return parameters;
	}


 
	//TODO far sparire "open"
	private final String commandInit = "LD_LIBRARY_PATH=/mpi/openmpi/lib /mpi/openmpi/bin/mpirun -q -np £1 ";
	private String mpiUser;
	private String commandFinal;
	private List<ClusterNode> workerHosts;
	private int procNumber;
	private String output;
	private GregorianCalendar launched;
	private GregorianCalendar ended;
	private String jobName, executable, parameters, outputFile;
	private UUID id;
	private JobState jobState;
	
	
	public enum JobState{
		RUNNING, ENDED, ERROR
	}
	
}
