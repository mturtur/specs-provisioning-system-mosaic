package eu.specs_project.provisioning.mosaic.failure_recovery;

public class PolicyParameters {
	
	private boolean retryScript, replaceNode;
	private int numberOfRetries, maxRetriesOfPolicy;
	
	public PolicyParameters(){
		super();
	}
	
	public PolicyParameters(boolean retryScript, boolean replaceNode,
			int numberOfRetries, int maxRetriesOfPolicy) {
		super();
		this.retryScript = retryScript;
		this.replaceNode = replaceNode;
		if(retryScript){
			this.numberOfRetries = numberOfRetries;
			this.maxRetriesOfPolicy=maxRetriesOfPolicy;
		}else{
			this.numberOfRetries=0;
			this.maxRetriesOfPolicy=0;
		}
	}

	public boolean isRetryScript() {
		return retryScript;
	}

	public boolean isReplaceNode() {
		return replaceNode;
	}

	public int getNumberOfRetries() {
		return numberOfRetries;
	}

	public void setRetryScript(boolean retryScript) {
		this.retryScript = retryScript;
	}

	public void setReplaceNode(boolean replaceNode) {
		this.replaceNode = replaceNode;
	}

	public void setNumberOfRetries(int numberOfRetries) {
		this.numberOfRetries = numberOfRetries;
	}

	public int getMaxRetriesOfPolicy() {
		return maxRetriesOfPolicy;
	}

	public void setMaxRetriesOfPolicy(int maxRetriesOfPolicy) {
		this.maxRetriesOfPolicy = maxRetriesOfPolicy;
	}
	
}
