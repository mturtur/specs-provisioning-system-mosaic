package eu.specs_project.provisioning.mosaic.failure_recovery;

import java.util.List;

import eu.specs_project.provisioning.mosaic.temp.ClusterNode;

public class RecoveryResult {
	
	private RecoveryOutcome outcome;
	private List<String> output;
	private ClusterNode newNode;
	
	public RecoveryResult(){
		outcome = RecoveryOutcome.SUCCEEDED;
	}	
	
	public RecoveryResult(RecoveryOutcome outcome, List<String> output,
			ClusterNode newNode) {
		super();
		this.outcome = outcome;
		this.output = output;
		this.newNode = newNode;
	}

	public RecoveryOutcome getOutcome() {
		return outcome;
	}

	public void setOutcome(RecoveryOutcome outcome) {
		this.outcome = outcome;
	}

	public List<String> getOutput() {
		return output;
	}

	public void setOutput(List<String> output) {
		this.output = output;
	}

	public ClusterNode getNewNode() {
		return newNode;
	}

	public void setNewNode(ClusterNode newNode) {
		this.newNode = newNode;
	}

	public enum RecoveryOutcome{
		SUCCEEDED, DESTROYED, REPLACED;
	}
	
}
