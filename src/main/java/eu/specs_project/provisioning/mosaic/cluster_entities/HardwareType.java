package eu.specs_project.provisioning.mosaic.cluster_entities;

import java.util.UUID;

public class HardwareType {

	public HardwareType(String providerHwId, String name){
		this.providerHwId = providerHwId;
		this.name = name;
	}
	
	public String getProviderHwId() {
		return providerHwId;
	}
	public void setProviderHwId(String providerHwId) {
		this.providerHwId = providerHwId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public UUID getId() {
		return id;
	}



	private final UUID id = UUID.randomUUID();
	private String providerHwId, name;
}
