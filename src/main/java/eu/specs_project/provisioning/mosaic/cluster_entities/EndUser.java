package eu.specs_project.provisioning.mosaic.cluster_entities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class EndUser{
	
	private final UUID id = UUID.randomUUID();
	
	public EndUser(){
		this.accounts=new ArrayList<UserCSPAccount>();
	}
	
	public EndUser(String name, String surname, String username, String password){
		this.name=name;
		this.surname=surname;
		this.username=username;
		this.password=password;
		this.accounts=new ArrayList<UserCSPAccount>();
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public List<UserCSPAccount> getAccounts() {
		return accounts;
	}
	public void setAccounts(List<UserCSPAccount> accounts) {
		this.accounts = accounts;
	}

	public void addAccount(UserCSPAccount account){
		accounts.add(account);
	}
	
	public UUID getId(){
		return id;
	}

	private String username;
	private String password;
	private String name;
	private String surname;
	private List <UserCSPAccount> accounts;
	

}
