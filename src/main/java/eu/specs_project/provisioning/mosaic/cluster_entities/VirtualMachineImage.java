package eu.specs_project.provisioning.mosaic.cluster_entities;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class VirtualMachineImage {
	
	private final UUID id = UUID.randomUUID();
	
	public VirtualMachineImage(String imageName, String imageId) {
	
		this.imageName=imageName;
		this.imageId=imageId;
		this.mpiEnvs = new ArrayList<UUID>();
	}
	
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public List<UUID> getMPIEnvs() {
		return mpiEnvs;
	}
	public void setMPIEnvs(List<UUID> mpienvs) {
		this.mpiEnvs = mpienvs;
	}
	
	public void addEnv(UUID env){
		mpiEnvs.add(env);
	}
	
	public UUID getId(){
		return id;
	}

	private String imageName;
	private String imageId;
	private List <UUID> mpiEnvs;

}
