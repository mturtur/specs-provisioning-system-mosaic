package eu.specs_project.provisioning.mosaic.cloudlets.frontend;
 
import com.google.gson.Gson;

import eu.mosaic_cloud.cloudlets.connectors.httpg.HttpgQueueRequestedCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnector;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnectorFactory;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.AmqpQueueConsumeCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnector;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnectorFactory;
import eu.mosaic_cloud.cloudlets.core.CallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.GenericCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.ICallback;
import eu.mosaic_cloud.cloudlets.core.ICloudletController;
import eu.mosaic_cloud.cloudlets.tools.DefaultAmqpQueueConsumerConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultCloudletCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultHttpgQueueConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultKvStoreConnectorCallback;
import eu.mosaic_cloud.connectors.httpg.HttpgRequestMessage;
import eu.mosaic_cloud.connectors.httpg.HttpgResponseMessage;
import eu.mosaic_cloud.platform.core.configuration.ConfigurationIdentifier;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.utils.JsonDataEncoder;
import eu.mosaic_cloud.platform.core.utils.PlainTextDataEncoder;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;
import eu.specs_project.provisioning.mosaic.cluster_entities.EndUser;
import eu.specs_project.provisioning.mosaic.cluster_entities.Provider;
import eu.specs_project.provisioning.mosaic.mpi.MPIEnvironment;

public class HTTPgwFrontend{
		
	public static class HttpgContext{
		ICloudletController<HttpgContext> cloudlet;
		IConfiguration configuration;
	//	IHttpgQueueConnector<String, String> gateway; utilizzando il mHTTPgw non serve
		IKvStoreConnector<EndUser, Void> usersBucket;
		IKvStoreConnector<Provider, Void> providersBucket;
		IKvStoreConnector<MPIEnvironment, Void> mpiEnvsBucket;
		IAmqpQueueConsumerConnector<String, Void> mHTTPGWconsumer;
	}
	
	
	/*
	public static final class UsersBucketCallback extends
				DefaultKvStoreConnectorCallback<HttpgContext, EndUser, Void>{

		public CallbackCompletion<Void> initializeSucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("users bucket initialized successfully.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("users bucket destroyed successfully.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> getSucceeded(final HttpgContext context, final KvStoreCallbackCompletionArguments<EndUser, Void> arguments){
			this.logger.info("key value fetch data succeeded.");
			EndUser u = arguments.getValue();
			return ICallback.SUCCESS;
		}
		

		public CallbackCompletion<Void> setSucceeded(HttpgContext context,
				KvStoreCallbackCompletionArguments<EndUser, Void> arguments) {
			this.logger.info("data stored succeessfully.");
			EndUser eu = (EndUser) arguments.getValue();
			this.logger.info("stored user '{}' ", eu.getName());
			return ICallback.SUCCESS;
		}

	}

	public static final class ProvidersBucketCallback extends
			DefaultKvStoreConnectorCallback<HttpgContext, Provider, Void>{

		public CallbackCompletion<Void> initializeSucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("providers bucket initialized successfully.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("providers bucket destroyed successfully.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> getSucceeded(final HttpgContext context, final KvStoreCallbackCompletionArguments<Provider, Void> arguments){
			this.logger.info("key value fetch data succeeded.");
			Provider provider = arguments.getValue();
			return ICallback.SUCCESS;
		}
		
		public CallbackCompletion<Void> setSucceeded(HttpgContext context,
				KvStoreCallbackCompletionArguments<Provider, Void> arguments) {
			this.logger.info("data stored succeessfully.");
			Provider p = (Provider) arguments.getValue();
			this.logger.info("stored provider '{}' ", p.getId().toString());
			return ICallback.SUCCESS;
		}
	}

	public static final class MPIEnvsBucketCallback extends
			DefaultKvStoreConnectorCallback<HttpgContext, String, Void>{

		public CallbackCompletion<Void> initializeSucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("MPI repositories bucket initialized successfully.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("MPI repositories bucket destroyed successfully.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> getSucceeded(final HttpgContext context, final KvStoreCallbackCompletionArguments<String, Void> arguments){
			this.logger.info("key value fetch data succeeded.");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> setSucceeded(HttpgContext context,
				KvStoreCallbackCompletionArguments<String, Void> arguments) {
			this.logger.info("data stored succeessfully.");
			String repo = (String) arguments.getValue();
			this.logger.info("stored repository '{}' ", repo);
			return ICallback.SUCCESS;
		}
	}
	*/
	
	public static final class AmqpConsumerCallback extends
			DefaultAmqpQueueConsumerConnectorCallback<HttpgContext, String, Void> {

		public CallbackCompletion<Void> initializeSucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("queue consumer connector initialized successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final HttpgContext context, final CallbackArguments arguments){
			this.logger.info("queue consumer connector destroyed successfully");
			return ICallback.SUCCESS;
		}
	
		public CallbackCompletion<Void> consume(final HttpgContext context, final AmqpQueueConsumeCallbackArguments<String> arguments){
			final String mess = new String(arguments.getMessage());
			this.logger.info("received message with content '"+mess+"'; acknowledging...");
			
			CommandWrapper a = new Gson().fromJson(mess, CommandWrapper.class);
			this.logger.info("command: " + a.getCommand().toString());
			
		//	context.mHTTPGWconsumer.acknowledge(arguments.getToken());
			return ICallback.SUCCESS;
		}
		
		public CallbackCompletion<Void> acknowledgeSucceeded(final HttpgContext context, final GenericCallbackCompletionArguments<Void> arguments){
			this.logger.info("acknowledge succeeded...");

			return ICallback.SUCCESS;
		}
		
	}
	
	public static final class HttpGatewayCallback
			extends DefaultHttpgQueueConnectorCallback<HttpgContext, String, String, Void>{
		@Override
		public CallbackCompletion<Void> requested (final HttpgContext context, final HttpgQueueRequestedCallbackArguments<String> arguments){
			final HttpgRequestMessage<String> request = arguments.getRequest ();
			final StringBuilder responseBody = new StringBuilder ();
			final HttpgResponseMessage<String> response = HttpgResponseMessage.create200 (request, responseBody.toString());
	/*		responseBody.append (String.format ("Cloudlet: %s\n", context.identity));
			responseBody.append (String.format ("HTTP version: %s\n", request.version));
			responseBody.append (String.format ("HTTP method: %s\n", request.method));
			responseBody.append (String.format ("HTTP path: %s\n", request.path));
		
			if (request.body != null) {
				responseBody.append ("HTTP body:\n");
				responseBody.append (request.body);
			} else
				responseBody.append ("HTTP body: empty\n");
			final HttpgResponseMessage<String> response = HttpgResponseMessage.create200 (request, responseBody.toString());
			context.gateway.respond(response);*/
			return (ICallback.SUCCESS);
		}
		@Override
		public CallbackCompletion<Void> respondSucceeded(HttpgContext context,
				GenericCallbackCompletionArguments<Void> arguments) {

			this.logger.info("respond succeeded");
			return ICallback.SUCCESS;
		}
}
	
	
	public static final class LifeCycleHandler extends DefaultCloudletCallback<HttpgContext>{
		
		public CallbackCompletion<Void> destroy (final HttpgContext context,
				final CloudletCallbackArguments<HttpgContext> arguments){
			this.logger.info ("destroying cloudlet and components...");
			return CallbackCompletion.createAndChained(/*context.gateway.destroy(),*/context.mHTTPGWconsumer.destroy(),
					context.usersBucket.destroy(), context.providersBucket.destroy(), context.mpiEnvsBucket.destroy());
		}
		
		
		public CallbackCompletion<Void> initialize (final HttpgContext context, final CloudletCallbackArguments<HttpgContext> arguments){
			context.cloudlet = arguments.getCloudlet ();
			final IConfiguration configuration = context.cloudlet.getConfiguration();
		//	final IConfiguration gatewayConfiguration = configuration.spliceConfiguration (ConfigurationIdentifier.resolveAbsolute ("gateway"));
			final IConfiguration consumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("queuemhttpgw.consumer"));
			final IConfiguration usersBucketConfiguration = configuration.spliceConfiguration (ConfigurationIdentifier.resolveAbsolute ("users.bucket"));
			final IConfiguration providersBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("providers.bucket"));
			final IConfiguration mpiEnvsConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("mpi.envs.bucket"));
			this.logger.info("initializing components...");
			context.usersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(usersBucketConfiguration,
					EndUser.class, JsonDataEncoder.create(EndUser.class), new DefaultKvStoreConnectorCallback<HttpgContext, EndUser, Void>(), context);
			context.providersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(providersBucketConfiguration,
					Provider.class, JsonDataEncoder.create(Provider.class), new DefaultKvStoreConnectorCallback<HttpgContext, Provider, Void>(), context);
			context.mpiEnvsBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(mpiEnvsConfiguration,
					MPIEnvironment.class, JsonDataEncoder.create(MPIEnvironment.class), new DefaultKvStoreConnectorCallback<HttpgContext, MPIEnvironment, Void>(), context);
			context.mHTTPGWconsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(consumerConfiguration,
					String.class, PlainTextDataEncoder.create(), new AmqpConsumerCallback(), context);
		/*	context.gateway = context.cloudlet.getConnectorFactory (IHttpgQueueConnectorFactory.class).create (gatewayConfiguration, 
					String.class, PlainTextDataEncoder.DEFAULT_INSTANCE, String.class, PlainTextDataEncoder.DEFAULT_INSTANCE, 
					new HttpGatewayCallback(), context);
*/
			return CallbackCompletion.createAndChained(/*context.gateway.initialize(),*/ context.mHTTPGWconsumer.initialize(),
					context.usersBucket.initialize(), context.providersBucket.initialize(),
					context.mpiEnvsBucket.initialize());
		}
	
		public CallbackCompletion<Void> initializeSucceeded(HttpgContext context,
				CloudletCallbackCompletionArguments<HttpgContext> arguments){
			this.logger.info("cloudlet initialized successfully");
			return (ICallback.SUCCESS);
		}
		
		public CallbackCompletion<Void> destroySucceeded(HttpgContext context,
				CloudletCallbackCompletionArguments<HttpgContext> arguments){
			this.logger.info("cloudlet destroyed successfully");
			return (ICallback.SUCCESS);
		}
	}
	
}
