package eu.specs_project.provisioning.mosaic.cloudlets;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

import org.slf4j.Logger;

import com.google.gson.Gson;

import eu.mosaic_cloud.cloudlets.connectors.executors.ExecutionFailedCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.executors.ExecutionSucceededCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.executors.IExecutor;
import eu.mosaic_cloud.cloudlets.connectors.executors.IExecutorFactory;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnector;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.IKvStoreConnectorFactory;
import eu.mosaic_cloud.cloudlets.connectors.kvstore.KvStoreCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.AmqpQueueConsumeCallbackArguments;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnector;
import eu.mosaic_cloud.cloudlets.connectors.queue.amqp.IAmqpQueueConsumerConnectorFactory;
import eu.mosaic_cloud.cloudlets.core.CallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackArguments;
import eu.mosaic_cloud.cloudlets.core.CloudletCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.GenericCallbackCompletionArguments;
import eu.mosaic_cloud.cloudlets.core.ICallback;
import eu.mosaic_cloud.cloudlets.core.ICloudletController;
import eu.mosaic_cloud.cloudlets.tools.DefaultAmqpQueueConsumerConnectorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultCloudletCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultExecutorCallback;
import eu.mosaic_cloud.cloudlets.tools.DefaultKvStoreConnectorCallback;
import eu.mosaic_cloud.connectors.queue.amqp.IAmqpMessageToken;
import eu.mosaic_cloud.platform.core.configuration.ConfigurationIdentifier;
import eu.mosaic_cloud.platform.core.configuration.IConfiguration;
import eu.mosaic_cloud.platform.core.utils.JsonDataEncoder;
import eu.mosaic_cloud.platform.core.utils.PlainTextDataEncoder;
import eu.mosaic_cloud.tools.callbacks.core.CallbackCompletion;
import eu.specs_project.provisioning.mosaic.failure_recovery.NullRecoveryPolicy;
import eu.specs_project.provisioning.mosaic.failure_recovery.PolicyParameters;
import eu.specs_project.provisioning.mosaic.mpi.MPICommand;
import eu.specs_project.provisioning.mosaic.mpi.MPIInfo;
import eu.specs_project.provisioning.mosaic.mpi.MPICommand.JobState;
import eu.specs_project.provisioning.mosaic.temp.Cluster;
import eu.specs_project.provisioning.mosaic.temp.ClusterNode;
import eu.specs_project.provisioning.mosaic.temp.ExtraContent;
import eu.specs_project.provisioning.mosaic.temp.Cluster.State;
import eu.specs_project.provisioning.mosaic.temp.Cluster.TargetScriptExecution;



public class MPICloudlet {
	
	private static final String[] masterScript = {
		"echo \'\tStrictHostKeyChecking no\' >> /etc/ssh/ssh_config",
		"wget ££",
		"tar -zxvf ££ -C /",
		"echo '/mpi *(rw,sync,no_root_squash)' >> /etc/exports",
		"/etc/init.d/iptables stop",
		"/etc/init.d/nfs restart"
	};
	private static int masterScriptReplaceRepo = 1;
	private static int masterScriptReplaceMPITar = 2;
	private static final String[] slaveScript = {
		"/etc/init.d/iptables stop",
		"mkdir /mpi",
		"chown ££:££ /mpi",
		"mount -t nfs ££:/mpi /mpi"
	};
	private static int slaveScriptReplaceMasterNodeIP = 3;
	private static int slaveScriptReplaceMPIUser = 2;
//	private static final String[] disableHostCheckingScript = {
//		"ssh-keygen -R ££",
//		"ssh-keyscan -H ££, ££ >> /.ssh/known_hosts",
//		"ssh-keyscan -H ££ >> ~/.ssh/known_hosts"
//	};
	
	
	public static final class MPIContext{
		ICloudletController<MPIContext> cloudlet;
		IConfiguration configuration;
		IExecutor<Void, UUID> configClusterExecutor, startJobExecutor;
		IKvStoreConnector<String, ExtraContent> clustersBucket;
		IKvStoreConnector<String, Void> mpiInfoBucket, mpiJobsBucket;
		IKvStoreConnector<PolicyParameters, MPIInfo> recoveryPoliciesBucket;
		IAmqpQueueConsumerConnector<HashMap, UUID> configClusterConsumer;
		IAmqpQueueConsumerConnector<HashMap, UUID> startJobConsumer;
		HashMap<UUID, IAmqpMessageToken> configClusterToken;
		HashMap<UUID, HashMap<String, String>> clusterAttributes, jobAttributes;
		String mpiUserUsername;
		int maxNodes;
	}
	
	public static final class FailurePoliciesKvStoreCallback extends
		DefaultKvStoreConnectorCallback<MPIContext, PolicyParameters, MPIInfo>{

		@Override
		public CallbackCompletion<Void> getSucceeded(MPIContext context,
				KvStoreCallbackCompletionArguments<PolicyParameters, MPIInfo> arguments) {
			MPIInfo info = arguments.getExtra();
			ExtraContent extra = new ExtraContent(info, arguments.getValue());
			context.clustersBucket.get(info.getId().toString(), extra);
			return ICallback.SUCCESS;
		}

	}
	
	public static final class ConfigClusterAmqpConsumerCallback extends
		DefaultAmqpQueueConsumerConnectorCallback<MPIContext, HashMap, UUID> {
		
		public CallbackCompletion<Void> initializeSucceeded(final MPIContext context, final CallbackArguments arguments){
			this.logger.info("config cluster queue consumer connector initialized successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final MPIContext context, final CallbackArguments arguments){
			this.logger.info("config cluster queue consumer connector destroyed successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> consume(final MPIContext context, final AmqpQueueConsumeCallbackArguments<HashMap> arguments){
			this.logger.info("MPI cloudlet: received config message from frontend...");
			final HashMap<String, String> attributes = arguments.getMessage();
			UUID id = UUID.fromString(attributes.get("id"));
			this.logger.info("***************"+id.toString());
			context.clusterAttributes.put(id, attributes);
		//	MPIInfo info = new MPIInfo(id, ConfigurationParameters.getValueByKey("mpi_user_username"));
			MPIInfo info = new MPIInfo(id, "mpi-user");
			info.setMPIUrl(attributes.get("mpi"));
			context.mpiInfoBucket.set(id.toString(), new Gson().toJson(info, MPIInfo.class));
			context.configClusterToken.put(id, arguments.getToken());
			context.recoveryPoliciesBucket.get("default_policy", info);
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> acknowledgeSucceeded(final MPIContext context, final GenericCallbackCompletionArguments<UUID> arguments){
			this.logger.info("configuration cluster message acknowledge succeeded...");		
			return ICallback.SUCCESS;
		}
	}
	
	public static final class ClusterKvStoreCallback extends
			DefaultKvStoreConnectorCallback<MPIContext, String, ExtraContent>{

		@Override
		public CallbackCompletion<Void> getSucceeded(MPIContext context,
				KvStoreCallbackCompletionArguments<String, ExtraContent> arguments) {
			this.logger.info("cluster get succeeded...");
			Cluster c = new Gson().fromJson(arguments.getValue(), Cluster.class);
			if(c.getClusterState()==State.CREATING){//richiesta config
				ExtraContent extra = arguments.getExtra();
				context.configClusterExecutor.execute(new ConfigClusterExecutor(context, c, extra.getInfo(), extra.getPolicy(), this.logger), c.getId());
			}else if(c.getClusterState()==State.READY){//richiesta start job
				HashMap<String, String> attributes = context.jobAttributes.get(c.getId());
				String params;
				List<ClusterNode> hosts = (List<ClusterNode>) c.getSlaveNodes();
				hosts.add(c.getMasterNode());
				ArrayList<ClusterNode> workers = new ArrayList<ClusterNode>();
				String hostsString = attributes.get("hosts");
				String[] hostsIds = hostsString.split(";");
				for(String s : hostsIds){
					for(ClusterNode n : hosts){
						if(s.equalsIgnoreCase(n.getId())){
							workers.add(n);
							break;
						}
					}
				}			
				if(attributes.get("parameters")==null)
					params="";
				else
					params = attributes.get("parameters");
				MPIInfo info = arguments.getExtra().getInfo();
				PolicyParameters policy = arguments.getExtra().getPolicy();
				String execUrl = attributes.get("executable");
				String[] execFileSplit = execUrl.split("/");
				MPICommand command = new MPICommand(attributes.get("jobName"), info.getMpiUserUsername(), workers, Integer.parseInt(attributes.get("procNumber")), execFileSplit[execFileSplit.length-1], params);
				command.setJobState(JobState.RUNNING);
				info.addJob(command.getId());
				context.mpiJobsBucket.set(command.getId().toString(), new Gson().toJson(command, MPICommand.class));
				context.mpiInfoBucket.set(info.getId().toString(), new Gson().toJson(info, MPIInfo.class));
				context.startJobExecutor.execute(new StartJobExecutor(context, c, execUrl, command, info , policy, logger), c.getId());
			}
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> getFailed(MPIContext context,
				KvStoreCallbackCompletionArguments<String, ExtraContent> arguments) {
		
			this.logger.info("*****fail*****: get cluster failed!!!!!");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arguments.getError().printStackTrace(pw);
			this.logger.info("******fail****** get cluster failed, reason:" +sw.toString());
			return ICallback.SUCCESS;
		}
	}
	
	
	public static class ConfigClusterExecutor implements Callable<Void>{
		
		private MPIContext context;
		private Cluster c;
		private MPIInfo info;
		private PolicyParameters policy;
		private Logger logger;
		
		public ConfigClusterExecutor(MPIContext context, Cluster c, MPIInfo info, PolicyParameters policy, Logger logger){
			super();
			this.context=context;
			this.c=c;
			this.info=info;
			this.policy=policy;
			this.logger=logger;
			this.logger.info("config cluster executor created...");
		}

		@Override
		public Void call() throws Exception {
		
			this.logger.info("configuration cluster execution in progress...");
			this.logger.info("cluster id: "+ c.getId().toString());
			this.logger.info("master node: "+c.getMasterNodeId());
			//RetryAndReplaceRecoveryPolicy configUserPolicy = new RetryAndReplaceRecoveryPolicy(policy.getNumberOfRetries(), policy.getMaxRetriesOfPolicy());
			HashMap<String, String> keys = c.createNewUser(info.getMpiUserUsername(), null, new NullRecoveryPolicy());
			if(keys==null){
				context.clustersBucket.set(c.getId().toString(), new Gson().toJson(c, Cluster.class));
				return null;
			}
			info.setPrivateKeyMPIUser(keys.get("privateKeyNewUser"));
			info.setPublicKeyMPIUser(keys.get("publicKeyNewUser"));
			String[] master, slave, tarSplit;
			master = masterScript.clone();
			slave = slaveScript.clone();
			String url = info.getMPIUrl();
			master[masterScriptReplaceRepo] = master[masterScriptReplaceRepo].replaceAll("££", url);
			tarSplit = url.split("/");
			master[masterScriptReplaceMPITar] = master[masterScriptReplaceMPITar].replaceAll("££", tarSplit[tarSplit.length-1]);
			slave[slaveScriptReplaceMPIUser] = slave[slaveScriptReplaceMPIUser].replaceAll("££", info.getMpiUserUsername());
			this.logger.info("********* cluster id: "+c.getId().toString()+" master priv IP: "+c.obtainMasterPrivateIP());
			this.logger.info("slave script line 3 before substitution: "+ slave[slaveScriptReplaceMasterNodeIP]);
			slave[slaveScriptReplaceMasterNodeIP] = slave[slaveScriptReplaceMasterNodeIP].replaceAll("££", c.obtainMasterPrivateIP());
			this.logger.info("slave script line 3 after substitution: "+ slave[slaveScriptReplaceMasterNodeIP]);
			NullRecoveryPolicy configClusterPolicy = new NullRecoveryPolicy();
			c.executeScriptAsRoot(TargetScriptExecution.MASTER, master, configClusterPolicy);
			c.executeScriptAsRoot(TargetScriptExecution.SLAVE, slave, configClusterPolicy);
//			List<ClusterNode> slaves = c.getSlaveNodes();
//			for(int i = 0; i<slaves.size(); i++){
//				hostChecking = disableHostCheckingScript;
//				for(int j = 0; j<hostChecking.length; j++){
//					hostChecking[j]=hostChecking[j].replaceAll("££", slaves.get(i).getPrivateIP());
//				}
//				c.executeScriptAsUser(TargetScriptExecution.MASTER, hostChecking, info.getMpiUserUsername(), info.getPrivateKeyMPIUser());				
//			}
			context.mpiInfoBucket.set(info.getId().toString(), new Gson().toJson(info, MPIInfo.class));
			c.setClusterState(Cluster.State.READY);
			context.clustersBucket.set(c.getId().toString(), new Gson().toJson(c, Cluster.class));
			return null;
		}
		
	}
	
	public static final class ConfigClusterExecutorCallback extends DefaultExecutorCallback<MPIContext, Void, UUID>{
		@Override
		public CallbackCompletion<Void> initializeSucceeded(MPIContext context,
				CallbackArguments arguments) {

			this.logger.info("config cluster executor initialized successfully");
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionSucceeded(MPIContext context,
				ExecutionSucceededCallbackArguments<Void, UUID> arguments) {

			this.logger.info("config cluster execution succeeded");
			UUID id = arguments.getExtra();
			context.configClusterConsumer.acknowledge(context.configClusterToken.get(id), id);
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionFailed(MPIContext context,
				ExecutionFailedCallbackArguments<UUID> arguments) {

			this.logger.info("********configuration cluster execution failed********: "+arguments.getException().getMessage());
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arguments.getException().printStackTrace(pw);
			this.logger.info("******configuration cluster execution failed******, reason:" +sw.toString());
			UUID id = arguments.getExtra();
			context.configClusterConsumer.acknowledge(context.configClusterToken.get(id), id);
			
			return ICallback.SUCCESS;
		}
	}
	
	public static final class StartJobAmqpConsumerCallback extends
		DefaultAmqpQueueConsumerConnectorCallback<MPIContext, HashMap, UUID> {
	
		public CallbackCompletion<Void> initializeSucceeded(final MPIContext context, final CallbackArguments arguments){
			this.logger.info("start job queue consumer connector initialized successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> destroySucceeded(final MPIContext context, final CallbackArguments arguments){
			this.logger.info("start job queue consumer connector destroyed successfully");
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> consume(final MPIContext context, final AmqpQueueConsumeCallbackArguments<HashMap> arguments){
			this.logger.info("Execute: received message from frontend...");
			final HashMap<String, String> attributes = arguments.getMessage();
			UUID id = UUID.fromString(attributes.get("clusterId"));
			context.jobAttributes.put(id, attributes);
			context.configClusterToken.put(id, arguments.getToken());
			context.mpiInfoBucket.get(id.toString());
			return ICallback.SUCCESS;
		}

		public CallbackCompletion<Void> acknowledgeSucceeded(final MPIContext context, final GenericCallbackCompletionArguments<Void> arguments){
			this.logger.info("start job message acknowledge succeeded...");
			return ICallback.SUCCESS;
		}
	}
	
	public static final class MPIInfoKvStoreCallback extends
		DefaultKvStoreConnectorCallback<MPIContext, String, Void>{

		@Override
		public CallbackCompletion<Void> getSucceeded(MPIContext context,
				KvStoreCallbackCompletionArguments<String, Void> arguments) {
			this.logger.info("mpi info get succeeded...");
			MPIInfo info = new Gson().fromJson(arguments.getValue(), MPIInfo.class);
			ExtraContent extra = new ExtraContent(info, null);
			context.clustersBucket.get(info.getId().toString(), extra);
			return ICallback.SUCCESS;
		}

		@Override
		public CallbackCompletion<Void> getFailed(MPIContext context,
				KvStoreCallbackCompletionArguments<String, Void> arguments) {

			this.logger.info("*****fail*****: get mpi info failed!!!!!");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arguments.getError().printStackTrace(pw);
			this.logger.info("******fail****** get mpi info failed, reason:" +sw.toString());
			return ICallback.SUCCESS;
		}
	}
	
	public static class StartJobExecutor implements Callable<Void>{
		
		private MPIContext context;
		private Cluster c;
		private String execUrl;
		private MPICommand command;
		private MPIInfo info;
		private PolicyParameters policy;
		private Logger logger;
		
		public StartJobExecutor(MPIContext context, Cluster c, String execUrl, MPICommand command, MPIInfo info, PolicyParameters policy, Logger logger){
			super();
			this.context=context;
			this.c=c;
			this.execUrl=execUrl;
			this.command=command;
			this.info = info;
			this.policy=policy;
			this.logger=logger;
			this.logger.info("job executor created...");
		}

		@Override
		public Void call() throws Exception {

			this.logger.info("job execution in progress...");
			String[] wget = {
					"wget "+this.execUrl+" --directory-prefix=/mpi",
					"chmod 744 /mpi/"+this.command.getExecutable()
			};
			NullRecoveryPolicy jobPolicy = new NullRecoveryPolicy();
			c.executeScriptAsUser(TargetScriptExecution.MASTER, wget, info.getMpiUserUsername(), info.getPrivateKeyMPIUser(), jobPolicy);
			String[] i = {command.getCommandFinal()};
			c.executeScriptAsUser(TargetScriptExecution.MASTER, i, info.getMpiUserUsername(), info.getPrivateKeyMPIUser(), jobPolicy); 
			String[] cat = {
					"cat " +command.getOutputFileName()
			};
			command.setOutput((c.executeScriptAsUser(TargetScriptExecution.MASTER, cat, info.getMpiUserUsername(), info.getPrivateKeyMPIUser(), jobPolicy)).get(c.getMasterNodeId()).get(0));
			command.setEnded((GregorianCalendar) GregorianCalendar.getInstance());
			command.setJobState(JobState.ENDED);
			context.mpiJobsBucket.set(command.getId().toString(), new Gson().toJson(command, MPICommand.class));
			info.addJob(command.getId());
			context.mpiInfoBucket.set(info.getId().toString(), new Gson().toJson(info, MPIInfo.class));
			return null;
		}
		
	}
	
	public static final class StartJobExecutorCallback extends DefaultExecutorCallback<MPIContext, Void, UUID>{
		@Override
		public CallbackCompletion<Void> initializeSucceeded(MPIContext context,
				CallbackArguments arguments) {

			this.logger.info("job executor initialized successfully");
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionSucceeded(MPIContext context,
				ExecutionSucceededCallbackArguments<Void, UUID> arguments) {

			this.logger.info("start job execution succeeded");
			UUID id = arguments.getExtra();
			context.startJobConsumer.acknowledge(context.configClusterToken.get(id), id);
			return ICallback.SUCCESS;
		}
		
		@Override
		public CallbackCompletion<Void> executionFailed(MPIContext context,
				ExecutionFailedCallbackArguments<UUID> arguments) {

			this.logger.info("********start job execution failed********: "+arguments.getException().getMessage());
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			arguments.getException().printStackTrace(pw);
			this.logger.info("******start job execution failed****** reason:" +sw.toString());
			return ICallback.SUCCESS;
		}
	}
	
	public static final class LifeCycleHandler
			extends DefaultCloudletCallback<MPIContext>{

		@Override
		public CallbackCompletion<Void> destroy(MPIContext context,
				CloudletCallbackArguments<MPIContext> arguments) {

			return CallbackCompletion.createAndChained(context.clustersBucket.destroy(), context.mpiInfoBucket.destroy(),
					context.configClusterConsumer.destroy(), context.startJobConsumer.destroy(), context.configClusterExecutor.destroy(),
					context.startJobExecutor.destroy(), context.mpiJobsBucket.destroy(), context.recoveryPoliciesBucket.destroy());
		}

		@Override
		public CallbackCompletion<Void> initialize(
				MPIContext context,
				CloudletCallbackArguments<MPIContext> arguments) {
			
			context.cloudlet = arguments.getCloudlet ();
			final IConfiguration configuration = context.cloudlet.getConfiguration();
			final IConfiguration configClusterConsumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("config.consumer"));
			final IConfiguration startJobConsumerConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("startjob.consumer"));
			final IConfiguration configClusterExecutorConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("executor"));
			final IConfiguration startJobExecutorConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("executor"));
			final IConfiguration clustersBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("clusters.bucket"));
			final IConfiguration mpiInfoBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("mpi.info.bucket"));			
			final IConfiguration mpiJobsBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("mpi.jobs.bucket"));			
			final IConfiguration recoveryPoliciesBucketConfiguration = configuration.spliceConfiguration(ConfigurationIdentifier.resolveAbsolute("recovery.policies.bucket"));
			this.logger.info("initializing MPI cloudlet components...");			
			context.configClusterToken = new HashMap<UUID, IAmqpMessageToken>();
			context.clusterAttributes = new HashMap<UUID, HashMap<String, String>>();
			context.jobAttributes = new HashMap<UUID, HashMap<String, String>>();
			context.configClusterConsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(configClusterConsumerConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new ConfigClusterAmqpConsumerCallback(), context);
			context.startJobConsumer = context.cloudlet.getConnectorFactory(IAmqpQueueConsumerConnectorFactory.class).create(startJobConsumerConfiguration,
					HashMap.class, JsonDataEncoder.create(HashMap.class), new StartJobAmqpConsumerCallback(), context);
			context.configClusterExecutor = context.cloudlet.getConnectorFactory(IExecutorFactory.class).create(configClusterExecutorConfiguration,
					new ConfigClusterExecutorCallback(), context);
			context.startJobExecutor = context.cloudlet.getConnectorFactory(IExecutorFactory.class).create(startJobExecutorConfiguration,
					new StartJobExecutorCallback(), context);
			context.clustersBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(clustersBucketConfiguration,
					String.class, PlainTextDataEncoder.create(), new ClusterKvStoreCallback(), context);		
			context.mpiInfoBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(mpiInfoBucketConfiguration,
					String.class, PlainTextDataEncoder.create(), new MPIInfoKvStoreCallback(), context);
			context.mpiJobsBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(mpiJobsBucketConfiguration,
					String.class, PlainTextDataEncoder.create(), new DefaultKvStoreConnectorCallback<MPIContext, String, Void>(), context);
			context.recoveryPoliciesBucket = context.cloudlet.getConnectorFactory(IKvStoreConnectorFactory.class).create(recoveryPoliciesBucketConfiguration,
					PolicyParameters.class, JsonDataEncoder.create(PolicyParameters.class), new FailurePoliciesKvStoreCallback(), context);
			
			return CallbackCompletion.createAndChained(context.mpiInfoBucket.initialize(), context.clustersBucket.initialize(),
					context.configClusterConsumer.initialize(), context.startJobConsumer.initialize(), context.configClusterExecutor.initialize(),
					context.startJobExecutor.initialize(), context.mpiJobsBucket.initialize(), context.recoveryPoliciesBucket.initialize());
		}
		
		public CallbackCompletion<Void> initializeSucceeded(MPIContext context,
				CloudletCallbackCompletionArguments<MPIContext> arguments){
			this.logger.info("MPI cloudlet initialized successfully");		
			return (ICallback.SUCCESS);
		}
		
		public CallbackCompletion<Void> destroySucceeded(MPIContext context,
				CloudletCallbackCompletionArguments<MPIContext> arguments){
			this.logger.info("MPI cloudlet destroyed successfully");
			return (ICallback.SUCCESS);
		}
		
	}
	
}
