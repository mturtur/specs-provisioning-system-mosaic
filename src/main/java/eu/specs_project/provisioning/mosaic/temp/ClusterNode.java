package eu.specs_project.provisioning.mosaic.temp;

public class ClusterNode {

	private String id, publicIP, privateIP;

	public ClusterNode(String id, String publicIP, String privateIP) {
		super();
		this.id = id;
		this.publicIP = publicIP;
		this.privateIP = privateIP;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPublicIP() {
		return publicIP;
	}

	public void setPublicIP(String publicIP) {
		this.publicIP = publicIP;
	}

	public String getPrivateIP() {
		return privateIP;
	}

	public void setPrivateIP(String privateIP) {
		this.privateIP = privateIP;
	}
	
}
