package eu.specs_project.provisioning.mosaic.temp;

import java.util.UUID;

public class ConfigStatus {

	private UUID id;
	private boolean continueForConfig;
	
	public ConfigStatus(){
		super();
	}
	
	public ConfigStatus(UUID id, boolean continueForConfig) {
		super();
		this.id = id;
		this.continueForConfig = continueForConfig;
	}

	public UUID getId() {
		return id;
	}

	public boolean isContinueForConfig() {
		return continueForConfig;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public void setContinueForConfig(boolean continueForConfig) {
		this.continueForConfig = continueForConfig;
	}
	
	
	
}
